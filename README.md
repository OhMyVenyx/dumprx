## qssi-user 12 SKQ1.211202.001 V13.0.3.0.SJFMIXM release-keys
- Manufacturer: xiaomi
- Platform: bengal
- Codename: citrus
- Brand: POCO
- Flavor: qssi-user
- Release Version: 12
- Id: SKQ1.211202.001
- Incremental: V13.0.3.0.SJFMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: POCO/citrus_global/citrus:12/RKQ1.211130.001/V13.0.3.0.SJFMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12-SKQ1.211202.001-V13.0.3.0.SJFMIXM-release-keys
- Repo: poco_citrus_dump
